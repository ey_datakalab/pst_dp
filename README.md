# PST_DP

## Main contributors

The main contributors (in alphabetic order) for this project are
 - Pauline Honoré
 - Jean Pillet
 - Aurélien Suizdak

## Goal

Our goal in this project is
- [x] train a GAN: discriminate real experiments from fake experiments
- [ ] create a 10th exp: to test that the model detects an already seen scene
- [ ] create an 11th exp: to test that the model detects a never seen scene


## How to setup

You will need a few things. First, cloen this repository, using something like `git clone git@gitlab.com:ey_datakalab/pst_dp.git`. Once this is done, please create a `raw_data` fodler in which we expect you to palce the raw data (`.txt` files).

Note that anything placed in the raw_data folder (also for the logs folder created by the software) are ignored by git so you can do whatever you wish in them.

## How to install

Once you went through the set-up, you can install the required python packages. In a conda environment, you can simply run `python -m src.partial_discharge` and it will give you the pip commands to use. Follow these steps:
 1. run `conda create -n pst python=3.9`
 2. run `conda activate pst`
 3. run `python -m src.partial_discharge` and follow the instructions from there

Note that these instructions won't work on a M1/M2 mac check [this link](https://caffeinedev.medium.com/how-to-install-tensorflow-on-m1-mac-8e9b91d93706).

## How to create a proper raw data folder

In order to have a code that can be used with future data extractions, we can assume that the folder contains .txt files and one json file containing a dict which maps file names to their corresponding class. For instance, assume we have files named :
```
01_Electrode sur céramique 1 mm.txt
02_Electrode sur céramique 2 mm.txt
03_Electrode sur téflon 1 mm.txt
04_Pointe avec contact - céramique 1 mm.txt
05_Pointe avec contact - céramique 2 mm.txt
06_Pointe avec contact - téflon 1 mm.txt
07_fil - céramique 1 mm.txt
08_fil - céramique 2 mm.txt
09_fil - téflon 1 mm.txt
```
then the json file associated should be
```
{
    "01_Electrode sur céramique 1 mm.txt" : 0,
    "02_Electrode sur céramique 2 mm.txt" : 1,
    "03_Electrode sur téflon 1 mm.txt" : 2,
    "04_Pointe avec contact - céramique 1 mm.txt" : 3,
    "05_Pointe avec contact - céramique 2 mm.txt" : 4,
    "06_Pointe avec contact - téflon 1 mm.txt" : 5,
    "07_fil - céramique 1 mm.txt" : 6,
    "08_fil - céramique 2 mm.txt" : 7,
    "09_fil - téflon 1 mm.txt" : 8
}
```

To create such json file, please run
```
python -m src.partial_discharge --create_json
```

## How to use

In order to use the code with CLI please run
```
python -m src.partial_discharge
```
