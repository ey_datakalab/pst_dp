import tensorflow as tf
import numpy as np
from typing import Tuple, List


def create_dataset(
    raw_data: List[np.ndarray],
    batch_size: int,
    training: bool = False,
    example_duration: int = 1000,
) -> tf.data.Dataset:
    """
    creates a dataset from raw data

    Args:
        x: testing inputs
        batch_size: training batch size
        training: boolean value to determine if we build a training set or not
        example_duration: definition of the duration of one sample (time duration)
    """
    slices = (
        [
            cpt
            for cpt in range(len(raw_data))
            for _ in range(len(raw_data[cpt]) - int(example_duration))
        ],
        [
            cpt
            for cpt_ in range(len(raw_data))
            for cpt in range(len(raw_data[cpt_]) - int(example_duration))
        ],
    )
    # slices indicates the number of 1000-sized slices per experience
    # slices = ([0, 0, ..., 1, 1, ..., 2, 2, ... ] i.e. index of experience in raw_data,
    #           [0, 1, ..., 0, 1, ..., 0, 1, ... ] i.e. x_th slice in each experience)

    print("starting conversion")
    tf_raw_data = tf.ragged.constant(raw_data, dtype=tf.float16)
    print("ended conversion")
    # convert raw_data to a tensor (no modif)

    time_range = tf.constant(value=list(range(example_duration)))
    # list of indices from 0 to duration-1 in tensor format

    indices = [0, 1, 3, 4]
    # we want to create batches of 1000 rows without taking into account the time which is only descriptive

    def load_sample(
        exp_idx: tf.Tensor, time_stamp: tf.Tensor
    ) -> Tuple[tf.Tensor, tf.Tensor]:
        # exp_idx is the number of the exp in tensor format
        # time_stamp is the starting row index of the sample
        exp_data = tf.gather(tf_raw_data, exp_idx)
        # exp_data is the data of the x_th experience
        x_time_stamps = time_range + time_stamp
        # x_time_stamps is a list of 1000 indices in tensor format
        x = tf.gather(exp_data, x_time_stamps)
        # x is the data for a sample starting at time_stamp
        x = tf.cast(tf.gather(x, tf.constant(indices), axis=-1), dtype=tf.float32)
        # we only include the column indices indicated in "indices" in the sample data
        return x

    dataset = tf.data.Dataset.from_tensor_slices(slices)
    # creates a dataset from "slices" created before
    dataset = dataset.prefetch(tf.data.AUTOTUNE)
    # inserts a dynamic buffer for faster loading of data

    if training:
        dataset = dataset.shuffle(len(slices[0]))
        # mélange les slices entre elles, tout en gardant une cohérence (index exp, x_th slice de cette exp)
        dataset = dataset.repeat()
        # recrée un dataset reshufflé à la suite et ce à l'infini

    dataset = dataset.map(
        load_sample,
        num_parallel_calls=tf.data.AUTOTUNE,
    )
    # applique la fonction load_sample sur tout le dataset
    # i.e. à partir des slices, on va récupérer des samples de taille 1000 avec les données exp
    # ils seront dans un ordre random autant intra-exp que inter-exp
    # num_parallel_calls permet de paralléliser de manière dynamique le calcul de la fonction sur le dataset

    dataset = dataset.batch(batch_size)
    # permet de combiner un nombre consécutif d'échantillons du dataset en batch
    return dataset
    # Ainsi, on récupère à la fin un dataset organisé en batch contenant "batch_size" échantillons de
    # taille 1000 choisis manière random à-travers toutes les expériences
