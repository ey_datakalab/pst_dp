from .loader import collect_data
from .iterator import create_dataset
import logging
import tensorflow as tf
import numpy as np
from typing import Tuple


def create_split_train_test(
    data_path: str,
    logger: logging.Logger,
    batch_size: int,
    example_duration: int = 1000,
    split: int = 0,
) -> Tuple[tf.data.Dataset, tf.data.Dataset]:
    """
    returns two datasets: one training set and one test set.

    Args:
        data_path: path to the raw data (folder)
        logger: logger for verbose recording
        batch_size: training batch size
        example_duration: definition of the duration of one sample (time duration)
        split: argument for cross validation
    """
    # collect the data inside the folder, raw_data is organised as followed :
    # raw_data = [array([data of first file]), array([data of second file]), etc]
    raw_data = collect_data(data_path=data_path, logger=logger)

    train_size = 0.9

    x_train = [np.copy(data[: int(train_size * len(data))]) for data in raw_data]
    x_test = [
        np.copy(data[int(train_size * len(data)) :, [0, 1, 3, 4]]) for data in raw_data
    ]
    x_test[split] = np.copy(raw_data[split][:, [0, 1, 3, 4]])
    x_train = np.delete(arr=x_train, obj=split)

    return (
        create_dataset(
            raw_data=x_train,
            batch_size=batch_size,
            training=True,
            example_duration=example_duration,
        ),
        x_test,
    )
