import numpy as np
import logging
import os
from typing import List


def collect_data(data_path: str, logger: logging.Logger) -> List[np.ndarray]:
    """
    simply extract data and convert from data_path.
    the output of this function is a tuple of two lists.
    The first list contains the data (X) which are numpy
    array. The second list contains the labels (Y) which
    is a simple digit corresponding to the class.

    In order to have a code that can be used with future
    data extractions, we can assume that the folder contains .txt files
    and one json file containing a dict which maps file names
    to their corresponding class (see readme.md for more details).

    Args:
        data_path: path to the raw data (folder)
        logger: logger for verbose recording
    """
    # établit le modèle du tableau de données : défintion du nom des colonnes et du type de données
    # contenues dans chacune de celle-ci
    dt = np.dtype(
        [
            ("Phase", float),
            ("Charge", float),
            ("Temps", float),
            ("Tension mesurée", float),
            ("Tension calculée", float),
        ]
    )

    # lecture du fichier dont on a fournit l'adresse et création d'un tableau de données sur le
    # modèle précédemment défini. On définit ',' comme le séparateur de colonnes
    all_files = [
        os.path.join(data_path, element)
        for element in os.listdir(data_path)
        if ".txt" in element
    ]
    data = []
    for file in all_files:
        tmp = np.loadtxt(file, delimiter=",").astype(np.float32)
        idces_to_remove = []
        for cpt, row in enumerate(tmp):
            if np.any(np.isnan(row)):
                idces_to_remove.append(cpt)
        tmp = np.delete(arr=tmp, obj=idces_to_remove, axis=0)
        data.append(tmp)

    # si on fait un pré-traitement attention à ne le faire qu'une fois !!!
    # choix du format de la sortie
    # faites des visu (reproduire les résultats du labo)
    return data


""" test code to see how the data is structured once collected """
# file_data = collect_data(
#     os.getcwd() + "\\raw_data")
# print(file_data)

# x = []
# x.append(file_data[0][0:3])
# print(x)
# print(len(x[0]))
