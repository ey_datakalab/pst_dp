import matplotlib.pyplot as plt
import numpy as np


def plot_sample(sample: np.ndarray, filename: str) -> None:
    plt.scatter(sample[:, 0], sample[:, 1])
    plt.savefig(filename)
    plt.close()
