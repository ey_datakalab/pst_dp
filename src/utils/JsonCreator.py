import os
import json
import logging


def create_json_for_dataset(data_path: str, logger: logging.Logger):
    """
    this function will ask the user to define
    the labels associated to each raw input data.

    Args:
        data_path: path to raw data
    """
    header = f"-- [\033[91mcreate json file for {data_path}\033[0m] --"
    print(header.center(76))
    logger.info(header.center(76))
    instructions = (
        "Please type the value of the label (should be an integer)."
        "\nTo validate the input press [\033[97menter\033[0m]"
    )
    print(instructions)
    logger.info(instructions)
    label_dict = {}
    length = max([len(key) for key in os.listdir(data_path)])
    for key in sorted(os.listdir(data_path)):
        request_string = f"please share label for [\033[93m{key}\033[0m]{''.center(length - len(key))}: "
        input_value = input(request_string)
        try:
            input_value = int(input_value)
        except ValueError:
            print(f"The provided input value: {input_value}, is not a number")
        label_dict[key] = input_value
        logger.info(f"{request_string}{input_value}")
    with open(os.path.join(data_path, "label_map.json"), "w") as f:
        json.dump(label_dict, f)
    result_str = (
        f"Saved the dict at \033[95m{os.path.join(data_path, 'label_map.json')}\033[0m."
    )
    print(result_str)
    logger.info(result_str)
