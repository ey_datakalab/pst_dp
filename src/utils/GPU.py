from typing import Any, List
import importlib.util
import sys
import os
import subprocess


def get_mdl() -> list:
    """
    returns the gpu names
    """
    line_as_bytes = subprocess.check_output("nvidia-smi -L", shell=True)
    lines = line_as_bytes.decode("ascii").splitlines()
    output = []
    for line in lines:
        _, line = line.split(":", 1)
        line, _ = line.split("(")
        output.append(line.strip())
    return output


def _bytes_to_megabytes(bytes):
    return round((bytes / 1024) / 1024, 2)


def check_if_a_module_exists(module_name: str) -> bool:
    """
    This function checks if a module can be loaded
    """
    if module_name in sys.modules:
        return True
    elif (importlib.util.find_spec(module_name)) is not None:
        return True
    else:
        return False


def print_gpu_specs(verbose: bool = False) -> List[bool]:
    """
    print name and memory available per gpu
    """
    if check_if_a_module_exists("nvidia_smi"):
        import nvidia_smi

        try:
            nvidia_smi.nvmlInit()
        except:
            return []

        gpu_to_use = []
        gpu_names = get_mdl()
        _NUMBER_OF_GPU = nvidia_smi.nvmlDeviceGetCount()
        for gpu_id in range(_NUMBER_OF_GPU):
            handle = nvidia_smi.nvmlDeviceGetHandleByIndex(gpu_id)
            info = nvidia_smi.nvmlDeviceGetMemoryInfo(handle)
            mo_free_memory = _bytes_to_megabytes(info.free)
            if verbose:
                print(
                    f'\033[92m{f"{gpu_names[gpu_id]}: {mo_free_memory}Mo".center(45)}\033[0m'
                )
            gpu_to_use.append(mo_free_memory >= 6000)
        nvidia_smi.nvmlShutdown()
        return gpu_to_use
    return []


def hardware_setup(gpu: str = "") -> Any:
    """
    enables gpu computations with tensorflow and
    returns the mirrored strategy if multiple gpus
    are dtected.

    Args:
        gpu: select one gpu to use
    """
    gpu_to_use = print_gpu_specs()
    gpu_to_use_str = ""
    for cpt, use_gpu in enumerate(gpu_to_use):
        if use_gpu:
            gpu_to_use_str += f"{cpt},"
    if len(gpu_to_use_str) > 0:
        gpu_to_use_str = gpu_to_use_str[:-1]
    if gpu != "" and gpu_to_use[int(gpu)]:
        gpu_to_use_str = gpu
    os.environ["CUDA_VISIBLE_DEVICES"] = gpu_to_use_str
    import tensorflow as tf

    physical_devices = tf.config.list_physical_devices("GPU")

    if len(physical_devices) == 0:
        print(f"\n---------------------------------------------")
        print(
            f'\033[91m{f"no gpu detected, using CPU mode.".center(45)}\033[0m')
        print(f"---------------------------------------------\n")
        mirrored_strategy = None
    else:
        print(f"\n---------------------------------------------")
        print_gpu_specs(verbose=True)
        for gpu in tf.config.list_physical_devices("GPU"):
            tf.config.experimental.set_memory_growth(gpu, True)
        print(f"---------------------------------------------\n")
        if len(physical_devices) >= 2:
            mirrored_strategy = tf.distribute.MirroredStrategy()
        else:
            mirrored_strategy = None
    return mirrored_strategy
