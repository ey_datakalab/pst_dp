import tensorflow as tf
from keras import layers


def create_discriminator(example_duration: int) -> tf.keras.Model:
    model = tf.keras.Sequential()
    model.add(layers.Flatten())
    model.add(layers.Dense(16, activation="relu", input_shape=(example_duration * 4,)))
    model.add(layers.Dense(16, activation="relu"))
    model.add(layers.Dense(1, activation="sigmoid"))
    return model


# Modèle tournant sur CNN :
# model = tf.keras.Sequential()
# model.add(layers.Conv2D(64, (3, 4), strides=(2, 0), input_shape=[1000, 4, 1]))
"on prend des filtres englobant les 4 colonnes sur 3 lignes et on les fait glisser de 2 lignes en 2 lignes"
# model.add(layers.LeakyReLU())
# model.add(layers.Dropout(0.3))

# model.add(layers.Conv2D(128, (3, 4), strides=(2, 0)))
# model.add(layers.LeakyReLU())
# model.add(layers.Dropout(0.3))

# model.add(layers.Flatten())
# model.add(layers.Dense(1, activation='sigmoid'))
