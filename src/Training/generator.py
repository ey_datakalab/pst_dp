"""
Our generator is a function G:

    G : X -> fake_X

We need to know the domain of X and fake_X. We assume that they share the same space.
Because real and fake examples live in the same space, we need to define what is a real experiment.

noise_dim doit faire la taille de que ce que l'on considère comme la taille critique d'une
expérience, tout en gardant à l'esprit qu'il faut que le générateur soit capable de créer un
échantillon "fictif" réaliste de cette taille, la difficulté augmentant avec sa taille
"""
import tensorflow as tf
from keras import layers

"""
Challenge: choosing the right kernel and stride size considering that the desired output shape is rectangular
"""


def create_generator(example_duration: int) -> tf.keras.Model:
    model = tf.keras.Sequential()
    model.add(layers.InputLayer((example_duration * 4,)))

    model.add(layers.Reshape((example_duration * 4, 1, 1)))
    assert model.output_shape == (None, example_duration * 4, 1, 1)
    model.add(layers.Dense(256, activation="relu"))

    model.add(layers.Reshape((example_duration * 2, 2, 256)))
    assert model.output_shape == (None, example_duration * 2, 2, 256)
    model.add(layers.Dense(128, activation="relu"))

    model.add(layers.Reshape((example_duration, 4, 128)))
    assert model.output_shape == (None, example_duration, 4, 128)
    model.add(layers.Dense(64, activation="relu"))

    model.add(layers.Dense(1, activation="tanh"))
    model.add(layers.Reshape((example_duration, 4, 1)))
    assert model.output_shape == (None, example_duration, 4, 1)

    return model


def mod(x):
    decimal = x - tf.floor(x)
    x_floor_mod = tf.math.floormod(x=x, y=360, name=None)
    x_mod = x_floor_mod + decimal
    return tf.stop_gradient(x_mod - x) + x


def generator_v2(example_duration: int) -> tf.keras.Model:
    input_noise = layers.Input((example_duration * 3,))
    input_time_start = layers.Input((1,))
    x = layers.Dense(256, activation="relu")(input_noise)
    x = layers.Dense(256, activation="relu")(x)
    x1 = layers.BatchNormalization()(x)
    x1 = layers.Dense(256, activation="relu")(x1)
    x1 = layers.Dense(example_duration)(x1)
    x1 = layers.Reshape((example_duration, 1, 1))(x1)
    x2 = layers.BatchNormalization()(x)
    x2 = layers.Dense(256, activation="relu")(x2)
    x2 = 2.2 * layers.Dense(example_duration, activation="tanh")(x2)
    x2 = layers.Reshape((example_duration, 1, 1))(x2)
    x3 = layers.BatchNormalization()(x)
    x3 = layers.Dense(256, activation="relu")(x3)
    x3 = 2.2 * layers.Dense(example_duration, activation="tanh")(x3)
    x3 = layers.Reshape((example_duration, 1, 1))(x3)
    ranges = tf.expand_dims(
        tf.linspace(start=0.0, stop=429.300781948, num=1000), axis=0
    )
    time_axis = mod(input_time_start + tf.cast(ranges, dtype=tf.float32))
    time_axis = tf.expand_dims(tf.expand_dims(time_axis, axis=-1), axis=-1)
    outputs = layers.Concatenate(axis=2)([time_axis, x1, x2, x3])
    model = tf.keras.Model((input_noise, input_time_start), outputs)
    return model


# Modèle tournant sur CNN :
# model = tf.keras.Sequential()

# model.add(layers.Dense(125*256, input_shape=(4000,)))
# model.add(layers.BatchNormalization())
# model.add(layers.LeakyReLU())

# model.add(layers.Reshape((125, 1, 256)))
# assert model.output_shape == (None, 125, 1, 256)

# model.add(layers.Conv2DTranspose(128, kernel_size=(3,1), strides=(2, 1), padding = 'same'))
# assert model.output_shape == (None, 250, 1, 128)
# model.add(layers.BatchNormalization())
# model.add(layers.LeakyReLU())

# model.add(layers.Conv2DTranspose(64, kernel_size=(3, 2), strides=(2, 2), padding = 'same'))
# assert model.output_shape == (None, 500, 2, 64)
# model.add(layers.BatchNormalization())
# model.add(layers.LeakyReLU())

# model.add(layers.Conv2DTranspose(1, kernel_size=(3, 4), strides=(2, 2), padding = 'same', activation='tanh'))
# assert model.output_shape == (None, 1000, 4, 1)
