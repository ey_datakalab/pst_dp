from .generator import create_generator, generator_v2
from .discirminator import create_discriminator
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np


def gan_training(
    batch_size: int,
    training_set: tf.data.Dataset,
    training_steps: int,
    example_duration: int,
):
    generator = generator_v2(example_duration=example_duration)
    discriminator = create_discriminator(example_duration=example_duration)
    discriminator.compile(metrics=["accuracy"])
    generator_optimizer = tf.keras.optimizers.Adam(1e-4)
    discriminator_optimizer = tf.keras.optimizers.Adam(1e-4)
    cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=False)

    def generator_loss(fake_output):
        return cross_entropy(tf.ones_like(fake_output), fake_output)

    def discriminator_loss(real_output, fake_output):
        real_loss = cross_entropy(tf.ones_like(real_output), real_output)
        fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
        total_loss = real_loss + fake_loss
        return total_loss

    @tf.function
    def train_step(images):
        noise = tf.random.normal(shape=(batch_size, 3 * example_duration))
        start_time = tf.random.uniform(shape=(batch_size, 1), minval=0, maxval=360)

        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            generated_images = generator((noise, start_time), training=True)

            real_output = discriminator(images, training=True)
            fake_output = discriminator(generated_images, training=True)

            gen_loss = generator_loss(fake_output)
            disc_loss = discriminator_loss(real_output, fake_output)

        gradients_of_generator = gen_tape.gradient(
            gen_loss, generator.trainable_variables
        )
        gradients_of_discriminator = disc_tape.gradient(
            disc_loss, discriminator.trainable_variables
        )

        generator_optimizer.apply_gradients(
            zip(gradients_of_generator, generator.trainable_variables)
        )
        discriminator_optimizer.apply_gradients(
            zip(gradients_of_discriminator, discriminator.trainable_variables)
        )
        return gen_loss, disc_loss

    gen_losses = []
    disc_losses = []
    for step, inputs in enumerate(training_set):
        # [AS] ne devrait-on pas insérer : train_step(inputs) ?
        # [EY] tout à fait, je ne l'avais pas pour ne pas faire planter le code pendant que vous étiez en train d'avancer sur le reste
        gen_loss_values, disc_los_values = train_step(inputs)
        gen_loss_value = np.mean(gen_loss_values.numpy())
        disc_los_value = np.mean(disc_los_values.numpy())
        print(
            f"\r{100*step/training_steps:.3f}% - {gen_loss_value:.3f} | {disc_los_value:.3f}",
            end="",
        )
        gen_losses.append(gen_loss_values)
        disc_losses.append(disc_los_value)
        if training_steps == step:
            break
    print("\r100%" + " " * 15)
    plt.plot(list(range(len(gen_losses))), gen_losses, label="gen loss")
    plt.plot(list(range(len(disc_losses))), disc_losses, label="disc loss")
    plt.legend()
    plt.savefig("training_loss.png")
    plt.close()
    discriminator.save("discriminator.h5")
    generator.save("generator.h5")
