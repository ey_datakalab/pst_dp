from ..utils.progress_bar import FancyProgressBar
from typing import Any
import tensorflow as tf
import numpy as np


class Evaluator:
    def __init__(self, data: Any, example_duration: int, split: int) -> None:
        self.all_exps = data
        self.example_duration = example_duration
        self.FP = 0
        self.FN = 0
        self.VP = 0
        self.VN = 0
        self.split = split

    def eval_seen_exps(self, model: tf.keras.Model) -> None:
        seen_exps = self.all_exps
        for exp_idx, exp in enumerate(seen_exps):
            if exp_idx != self.split:
                pbar = FancyProgressBar(
                    total_steps=len(exp) - self.example_duration,
                    base_str=f"Test on exp{exp_idx}",
                )
                for cpt in range(len(exp) - self.example_duration):
                    pbar()
                    inputs = np.expand_dims(
                        exp[cpt : self.example_duration + cpt, :], axis=0
                    )
                    preds = model(inputs, training=False)
                    pred = np.round(preds.numpy())[0][0]
                    if pred == 1:
                        self.VP += 1
                    else:
                        self.FN += 1
                pbar(progression_complete=True)

    def eval_unseen_exps(self, model: tf.keras.Model) -> None:
        unseen_exps = self.all_exps[self.split]
        pbar = FancyProgressBar(
            total_steps=len(unseen_exps) - self.example_duration,
            base_str=f"Test on exp{self.split}",
        )
        for cpt in range(len(unseen_exps) - self.example_duration):
            pbar()
            inputs = np.expand_dims(
                unseen_exps[cpt : self.example_duration + cpt, :], axis=0
            )
            preds = model(inputs, training=False)
            pred = np.round(preds.numpy())[0][0]
            if pred == 1:
                self.FP += 1
            else:
                self.VN += 1
        pbar(progression_complete=True)

    def compression_matrix(self) -> str:
        str_vn = str(self.VN)
        str_vp = str(self.VP)
        str_fn = str(self.FN)
        str_fp = str(self.FP)
        col1 = max(len(str_vp), len(str_fn))
        col2 = max(len(str_vn), len(str_fp))
        matrix = ""
        matrix += str_vp.center(col1) + " | " + str_fp.center(col2) + "\n"
        matrix += str_fn.center(col1) + " | " + str_vn.center(col2) + "\n"
        return matrix

    def get_matrix(self) -> np.array:
        return np.array([[self.VP, self.FP], [self.FN, self.VN]])

    def __call__(self, model: tf.keras.Model, *args: Any, **kwds: Any) -> Any:
        self.eval_seen_exps(model=model)
        self.eval_unseen_exps(model=model)
        return self.compression_matrix()
