"""
This project studies partial discharges in the context of a PST
at Centrale-Supelec.
"""

if __name__ == "__main__":
    import os

    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
    import argparse

    parser = argparse.ArgumentParser(
        usage=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument(
        "--logger",
        nargs="?",
        type=str,
        default="logs.log",
        help="path to save the logs (if you use the default value the file will be overwritten)",
    )
    parser.add_argument(
        "--path",
        nargs="?",
        type=str,
        default=os.path.join(os.path.dirname(os.path.dirname(__file__)), "raw_data"),
        help="path to the raw data",
    )
    parser.add_argument(
        "--batch_size",
        nargs="?",
        type=int,
        default=32,
        help="training batch size",
    )
    parser.add_argument(
        "--n",
        nargs="?",
        type=int,
        default=1000,
        help="definition of the duration of one sample (time duration)",
    )
    parser.add_argument(
        "--create_json",
        action="store_true",
        default=False,
        help="run the unit test",
    )
    parser.add_argument(
        "--cross_val",
        action="store_true",
        default=False,
        help="runs the cross-validation",
    )
    args = parser.parse_args()
    from .utils.GetLogger import get_logger
    from .utils.Clear import remove_chache_folders
    from .utils.Packages import check_packages

    logger = get_logger(log_file=args.logger)
    check_packages(logger=logger)
    if args.create_json:
        from .utils.JsonCreator import create_json_for_dataset

        create_json_for_dataset(data_path=args.path, logger=logger)
        remove_chache_folders()
        quit()
    import logging
from typing import Tuple
from .utils.GPU import hardware_setup

hardware_setup()
from .Training.train import gan_training
from .Data.split import create_split_train_test
from .Test.one_eval import Evaluator
import tensorflow as tf


def train_a_neural_netwrok(
    batch_size: int,
    data_path: str,
    logger: logging.Logger,
    example_duration: int = 1000,
    training_steps: int = 2000,
    split: int = -1,
) -> Tuple[str, int]:
    """
    This function will be updated through time.
    We need a path to the raw data fodler from which we will 1) extract
    the data, 2) pre-process it 3) create a train set and test set.
    Then, we define the network to train and its task.
    Finally we implement the training loop before saving the final product.

    Args:
        batch_size: training batch size
        data_path: path to the raw data
        logger: logger for verbose recording
        example_duration: definition of the duration of one sample (time duration)
        training_steps: total number of training steps
        split: argument for cross validation
    """
    training_set, test_set = create_split_train_test(
        data_path=data_path,
        logger=logger,
        batch_size=batch_size,
        example_duration=example_duration,
        split=split,
    )
    gan_training(
        batch_size=batch_size,
        training_set=training_set,
        training_steps=training_steps,
        example_duration=example_duration,
    )
    discriminator = tf.keras.models.load_model("discriminator.h5")
    discriminator.compile(metrics=["accuracy"])
    eval_obj = Evaluator(data=test_set, example_duration=example_duration, split=split)
    output_str = eval_obj(model=discriminator)
    print(output_str)
    return eval_obj.get_matrix(), len(test_set)


def cross_validation(
    batch_size: int,
    data_path: str,
    logger: logging.Logger,
    example_duration: int = 1000,
    training_steps: int = 2000,
) -> str:
    """
    This function implements a cross validation on the dataset.

    Args:
        batch_size: training batch size
        data_path: path to the raw data
        logger: logger for verbose recording
        example_duration: definition of the duration of one sample (time duration)
        training_steps: total number of training steps
    """
    conf_mat, num_splits = train_a_neural_netwrok(
        batch_size=batch_size,
        data_path=data_path,
        logger=logger,
        example_duration=example_duration,
        training_steps=training_steps,
        split=0,
    )
    for split in range(1, num_splits):
        tmp_mat, _ = train_a_neural_netwrok(
            batch_size=batch_size,
            data_path=data_path,
            logger=logger,
            example_duration=example_duration,
            training_steps=training_steps,
            split=split,
        )
        conf_mat += tmp_mat
    str_vn = str(conf_mat[1, 1])
    str_vp = str(conf_mat[0, 0])
    str_fn = str(conf_mat[1, 0])
    str_fp = str(conf_mat[0, 1])
    col1 = max(len(str_vp), len(str_fn))
    col2 = max(len(str_vn), len(str_fp))
    matrix = ""
    matrix += str_vp.center(col1) + " | " + str_fp.center(col2) + "\n"
    matrix += str_fn.center(col1) + " | " + str_vn.center(col2) + "\n"
    print(matrix)
    return matrix


if __name__ == "__main__":
    if args.cross_val:
        cross_validation(
            batch_size=args.batch_size,
            data_path=args.path,
            logger=logger,
            example_duration=args.n,
        )
    else:
        train_a_neural_netwrok(
            batch_size=args.batch_size,
            data_path=args.path,
            logger=logger,
            example_duration=args.n,
        )
    remove_chache_folders()
